﻿ I have decided to organize the thermometer solution in the following manner:
 
 Thermometer -> Readers -> Sensors
 
 Thermometer is a simple data receiver and publisher. The class is intended for a single purpose - to obtain the reading and pass it along to all subscribed readers.
 
 Readers are objects that process thermometer readings. There could be readers for multiple purposes. Some of them are to register a reading and store it in the database.
 Some of them are to analyze the data as the readings come along. And some of them are to pass the data further - to the sensors. This is the type of the reader used in this solition.
 
 Sensors are classes that check for conditions to become activated. Each time a reading is passed from the thermometer to the temperature reader that works with sensors, 
 every sensor is notified of that new reading. Sensors then perform validation logic and, if the conditions are met, their activation method is called. 
 
 There could be a variety of sensors - some watch for a certain threshold, some have additional requirements, such as the direction of the temperature change (Implemented in this solution)
 
 There could be also other types of sensors that can be easily integrated with the solution - anomaly detectors, fast temperature change detectors, etc.

 Certain things in this solution might seem as overengineered; the reason for that is I tried to demonstrate the knowledge of language and patterns. 
 The real-life design would be matching the requirements closer and might have been implemented in a simpler way.