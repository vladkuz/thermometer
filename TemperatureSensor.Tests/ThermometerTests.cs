﻿using Xunit;

using Moq;

using Thermometer.Interfaces;
using Thermometer.Sensors;

namespace Thermometer.Tests
{
    public class ThermometerTests
    {
        /// <summary>
        /// Main test to verify the data flow between the thermometer and sensors is performed as intended
        /// </summary>
        [Fact]
        public void Can_Add_Reader_And_Get_Reading()
        {
            var readerMock = new Mock<ITemperatureReader>();
            var logger = new Mock<ILogger>();
            var testThermometer = new Thermometer(logger.Object);

            testThermometer.SubscribeReader(readerMock.Object);
            testThermometer.GetReading(new Temperature { Value = 1.23f });

            readerMock.Verify(x => x.NewReading(It.IsAny<TemperatureReading>()), Times.Exactly(1));
        }
    }

    public class SensorTests
    {
        Thermometer thermometer;
        TemperatureReader reader;

        /// <summary>
        /// Tests setup
        /// </summary>
        public SensorTests()
        {
            var logger = new Moq.Mock<ILogger>();

            thermometer = new Thermometer(logger.Object);
            reader = new TemperatureReader(logger.Object);

            thermometer.SubscribeReader(reader);
        }

        [Fact]
        public void Test_Threshold_With_No_Tolerance_Indicator()
        {
            // Arrange
            var activationCount = 0;

            // Simple threshold sensor - activates when the temperature reaches zero
            var freezingSensor = new ThresholdSensor(new Temperature { Value = 0 }, new Temperature { Value = 0 })
            {
                OnActivation = (reading) => activationCount++
            };

            reader.AddSensor(freezingSensor);

            // Act
            FeedThermometer();

            // Assert
            Assert.Equal(4, activationCount);
        }

        [Fact]
        public void Test_Threshold_With_Tolerance_Indicator()
        {
            // Arrange
            var activationCount = 0;

            // Create a sensor that would activate if the temperature reaches zero and would not activate if the temperature then fluctuates within the tolerance
            var freezingSensor = new ThresholdSensor(new Temperature { Value = 0 }, new Temperature { Value = 0.5f })
            {
                OnActivation = (reading) => activationCount++
            };

            reader.AddSensor(freezingSensor);
            // Act
            FeedThermometer();

            // Assert
            Assert.Equal(2, activationCount);
        }

        [Fact]
        public void Test_Threshold_And_Direction_Indicator()
        {
            // Arrange
            var activationCount = 0;
            // Create a sensor that would check if the freezing temperature is reached and the direction of the change is from higher temperatures to lower.
            // Use tolerance as well
            var freezingSensor = new ThresholdAndGradientSensor(new Temperature { Value = 0 }, new Temperature { Value = 0.5f }, Gradient.Negative)
            {
                OnActivation = (reading) => activationCount++
            };

            reader.AddSensor(freezingSensor);

            // Act
            FeedThermometer();

            // Assert
            Assert.Equal(1, activationCount);
        }

        /// <summary>
        /// Provides with fake readings
        /// </summary>
        private void FeedThermometer()
        {
            thermometer.GetReading(new Temperature { Value = 1.5f });
            thermometer.GetReading(new Temperature { Value = 1.0f });
            thermometer.GetReading(new Temperature { Value = 0.5f });
            thermometer.GetReading(new Temperature { Value = 0.0f });   // threshold should be reached here, if direction is 'decreasing'
            thermometer.GetReading(new Temperature { Value = -0.5f });
            thermometer.GetReading(new Temperature { Value = 0.0f });   // if tolerance is 0.5 or more, threshold should not be reached here
            thermometer.GetReading(new Temperature { Value = 0.5f });
            thermometer.GetReading(new Temperature { Value = 0.0f });   // if tolerance is 0.5 or more, threshold should not be reached here
            thermometer.GetReading(new Temperature { Value = -1.0f });
            thermometer.GetReading(new Temperature { Value = -1.5f });
            thermometer.GetReading(new Temperature { Value = -1.0f });
            thermometer.GetReading(new Temperature { Value = 0.0f });   // threshold should be reached here, if directions is 'increasing'
            thermometer.GetReading(new Temperature { Value = 0.5f });
        }
    }
}
