﻿using Xunit;

namespace Thermometer.Tests
{
    /// <summary>
    /// Testing Temperature object and its methods
    /// </summary>
    public class TemperatureTests
    {
        [Theory]
        [InlineData(0, 32f)]
        [InlineData(100f, 212f)]
        public void Can_Convert_To_Fahrenheit(float celcius, float fahrenheit)
        {
            var temp = new Temperature { Value = celcius, Scale = TemperatureScale.Celsius };

            Assert.Equal(fahrenheit, temp.ToFahrenheit());
        }

        [Theory]
        [InlineData(32f, 0f)]
        [InlineData(212f, 100f)]
        public void Can_Convert_To_Celcius(float fahrenheit, float celcius)
        {
            var temp = new Temperature { Value = fahrenheit, Scale = TemperatureScale.Fahrenheit };

            Assert.Equal(celcius, temp.ToCelcius());
        }

        [Fact]
        public void Can_Compare_Temperature()
        {
            var temp1 = new Temperature { Value = 0 };
            var temp2 = new Temperature { Value = 0 };
            var temp3 = new Temperature { Value = 10f };

            Assert.Equal(temp2, temp1);
            Assert.True(temp2 == temp1);
            Assert.False(temp2 != temp1);

            Assert.NotEqual(temp1, temp3);
            Assert.False(temp1 == temp3);
            Assert.True(temp2 != temp3);

        }
    }
}
