﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thermometer.Interfaces;

namespace Thermometer
{
    /// <summary>
    ///
    /// 
    /// </summary>
    public class Thermometer
    {
        private readonly List<ITemperatureReader> readers;
        private readonly ILogger logger;

        public Thermometer(ILogger logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.readers = new List<ITemperatureReader>();
        }

        public void GetReading(Temperature temp)
        {
            if ( temp == null)
            {
                // log an invalid reading or ignore
                return;
            }

            // Register the temperature with the current date/time (make it a reading)
            var reading = new TemperatureReading(temp); 

            NotifyReaders(reading);
        }

        public void SubscribeReader(ITemperatureReader reader)
        {
            if (reader != null)
            {
                readers.Add(reader);
            }
        }

        /// <summary>
        /// Announces new reading to all readers
        /// </summary>
        /// <param name="reading"></param>
        private void NotifyReaders(TemperatureReading reading)
        {
            Parallel.ForEach(readers, (reader) =>
            {
                try
                {
                    reader.NewReading(reading);
                }
                catch(Exception ex)
                {
                    logger.LogException("Error while notifying a reader.", ex);
                }
            });
        }

    }
}
