﻿using System.Collections.Generic;

namespace Thermometer.Sensors
{
    /// <summary>
    /// Special threshold sensor that activates only when the reading changes from a certain direction
    /// </summary>
    public class ThresholdAndGradientSensor : ThresholdSensor
    {
        private TemperatureReading lastReading;
        public Gradient Gradient { get; set; }

        public ThresholdAndGradientSensor(Temperature threshold, Temperature tolerance, Gradient gradient) : base(threshold, tolerance)
        {
            Gradient = gradient;
        }

        public override bool Validate(TemperatureReading reading)
        {
            // Check if the threshold has been reached
            var thresholdReached = base.Validate(reading);

            // Check if the direction is as required
            var isDirectionValid = Gradient == lastReading?.CompareTo(reading);

            // Save the reading to use it next time
            lastReading = reading;

            // Sensor activates only if both threshold and direction conditions are met
            return thresholdReached && isDirectionValid;
        }
    }

    /// <summary>
    /// Values indicating if the temperature is decreasing, increasing, or flat.
    /// </summary>
    public enum Gradient
    {
        Negative, Positive, Zero
    }

    /// <summary>
    /// Extension helper to perform temperatures comparison
    /// </summary>
    public static class TemperatureGradientExtension
    {
        public static Gradient CompareTo(this Temperature temperature, Temperature newTemperature)
        {
            return (temperature == newTemperature) ? Gradient.Zero :
                (temperature.ToCelcius() < newTemperature.ToCelcius() ? Gradient.Positive : Gradient.Negative);
        }
    }
}
