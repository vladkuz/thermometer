﻿using System;


namespace Thermometer.Sensors
{
    /// <summary>
    /// Abstract Sensor class to subscribe to the reader. 
    /// The reader would perform a condition check and, if the condition is met, the activation event will be triggered
    /// </summary>
    public abstract class Sensor
    {
        // Object descriptive properties
        public string Name { get; set; }
        public string Description { get; set; }
        
        // Method to validate if the reading warrants an activation
        public abstract bool Validate(TemperatureReading reading);

        // Assign your action to this method to be informed 
        public Action<TemperatureReading> OnActivation { get; set; }
    }
}
