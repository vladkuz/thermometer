﻿using System;

namespace Thermometer.Sensors
{
    /// <summary>
    /// Simple threshold sensor. Activates when a threshold reached (within tolerance)
    /// </summary>
    public class ThresholdSensor : Sensor
    {
        public Temperature Threshold { get; set; }
        public Temperature Tolerance { get; set; }

        protected bool IsActive;    // flag indicates if the sensor has reached the threshold 

        public ThresholdSensor(Temperature threshold, Temperature tolerance)
        {
            if ( tolerance.Value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(tolerance), "Tolerance value must be positive");
            }

            Threshold = threshold;
            Tolerance = tolerance;

            IsActive = false;

            Name = "Threshold sensor";
            Description = $"Senses when threshold {threshold} {(tolerance.Value != 0 ? $"(±{tolerance.Value})" : "")} has been reached.";
        }

        /// <summary>
        /// Check the activation conditions
        /// </summary>
        /// <param name="reading"></param>
        /// <param name="pastReadings"></param>
        /// <returns></returns>
        public override bool Validate(TemperatureReading reading)
        {
            // Checks if the reading is within the tolerance, indicating that the fluctuation is insignificant
            var alreadyActive = IsActive;

            // Set the active flag. If the reading goes away from the threshold then the sensor is not active anymore and 
            // the next time the threshold is reached, the sensor will activate again.
            // If the reading is within the tolerance and the sensor is already active, then the activation will not occur
            IsActive = IsThresholdReached(reading);

            // Return true only if the threshold has been reached just now
            return (alreadyActive ? false : IsActive);
        }

        /// <summary>
        /// Checks if the reading is at the threshold within the tolerance
        /// </summary>
        /// <param name="reading"></param>
        /// <returns></returns>
        private bool IsThresholdReached(TemperatureReading reading)
        {
            return reading.ToCelcius() >= Threshold.ToCelcius() - Tolerance.ToCelcius() && reading.ToCelcius() <= Threshold.ToCelcius() + Tolerance.ToCelcius();
        }
    }

}
