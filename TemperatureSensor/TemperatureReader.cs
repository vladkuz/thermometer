﻿using System;
using System.Collections.Generic;
using Thermometer.Sensors;
using Thermometer.Interfaces;
using System.Threading.Tasks;

namespace Thermometer
{
    /// <summary>
    /// Temperature reader class responsible to obtain a reading and notify all sensors of that new reading
    /// </summary>
    public class TemperatureReader : ITemperatureReader
    {
        public bool IgnoreSameValues { get; set; }  // If set to true, then readings with the same value as the last will be ignored and no sensors will be called.
        public bool ParallelizeNotifications { get; set; } // Feature flag - sensors can be notified using multiple threads if the sensor processes are expected to be slow

        private readonly List<Sensor> sensors;
        private readonly ILogger logger;
        private TemperatureReading lastReading = null;

        public TemperatureReader(ILogger logger)
        {
            this.logger = logger;
            this.sensors = new List<Sensor>();
        }

        /// <summary>
        /// Adds the sensor to the list
        /// </summary>
        /// <param name="sensor"></param>
        public void AddSensor(Sensor sensor)
        {
            if (sensor != null)
            {
                sensors.Add(sensor);
            }
        }

        /// <summary>
        /// This method is called each time a new reading arrives. 
        /// </summary>
        /// <param name="reading"></param>
        public void NewReading(TemperatureReading reading)
        {
            // To prevent excessive calls there is a feature that allows the reader to ignore all readings with the same value 
            // and notify sensons only if the reading's value has changed
            if (IgnoreSameValues) 
            {
                if (lastReading?.ToCelcius() == reading.ToCelcius())
                    return;
                lastReading = reading;
            }

            NotifySensors(reading);
        }


        /// <summary>
        /// Loops through all sensors and notifies them of the new reading
        /// </summary>
        /// <param name="reading"></param>
        private void NotifySensors(TemperatureReading reading)
        {
            if (ParallelizeNotifications)
            {
                Parallel.ForEach(sensors, sensor => NotifySensor(sensor, reading));
            }
            else
            {
                foreach(var sensor in sensors)
                {
                    NotifySensor(sensor, reading);
                }
            }
        }

        private void NotifySensor(Sensor sensor, TemperatureReading reading)
        {
            try
            {
                if (sensor.Validate(reading))
                {
                    // Trigger the sensor
                    sensor.OnActivation?.Invoke(reading);
                }
            }
            catch (Exception ex)
            {
                logger.LogException("Exception while notifying a sensor.", ex);
            }

        }
    }
}
