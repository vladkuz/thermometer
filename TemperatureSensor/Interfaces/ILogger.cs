﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thermometer.Interfaces
{
    /// <summary>
    /// Simple logger interface
    /// </summary>
    public interface ILogger
    {
        void LogException(string errorMessage, Exception ex);
    }
}
