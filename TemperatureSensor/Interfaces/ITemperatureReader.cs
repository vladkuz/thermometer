﻿namespace Thermometer.Interfaces
{
    public interface ITemperatureReader
    {
        void NewReading(TemperatureReading reading);
    }
}
