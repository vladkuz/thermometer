﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thermometer
{
    /// <summary>
    /// Temperature at certain point in time
    /// </summary>
    public class TemperatureReading : Temperature
    {
        public DateTimeOffset Timestamp { get; set; }

        public TemperatureReading(Temperature temp)
        {
            Value = temp.Value;
            Scale = temp.Scale;
            Timestamp = DateTimeOffset.UtcNow;
        }
    }
}
