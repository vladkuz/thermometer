﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thermometer
{
    public enum TemperatureScale
    {
        Celsius, Fahrenheit
    }

    // Temperature value object
    public class Temperature
    {
        public float Value { get; set; }
        public TemperatureScale Scale { get; set; }

        public Temperature()
        {
            Scale = TemperatureScale.Celsius;
        }

        // Conversion method
        public float ToFahrenheit()
        {
            return Scale == TemperatureScale.Celsius ? Value * 1.8f + 32 : Value;  
        }

        // Conversion method
        public float ToCelcius()
        {
            return Scale == TemperatureScale.Fahrenheit ? (Value - 32) / 1.8f : Value;
        }

        // Overrides to help with temperature object comparison
        public override bool Equals(object obj)
        {
            var temperature = obj as Temperature;
            return temperature != null &&
                   Value == temperature.Value &&
                   Scale == temperature.Scale;
        }

        public override int GetHashCode()
        {
            var hashCode = -1260670453;
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + Scale.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Temperature temp1, Temperature temp2)
        {
            if (ReferenceEquals(temp1, temp2))
            {
                return true;
            }

            if (temp1 is null || temp2 is null)
            {
                return false;
            }

            return temp1.ToCelcius() == temp2.ToCelcius();
        }

        public static bool operator !=(Temperature temp1, Temperature temp2)
        {
            return !(temp1 == temp2);
        }
    }

}
